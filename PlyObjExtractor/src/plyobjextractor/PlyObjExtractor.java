package plyobjextractor;

import java.io.File;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;
import metadata.FileMetadata;

public class PlyObjExtractor extends Extractor {

	private static final String QUEUE_NAME = "medici_ply_obj";
	private static final String EXCHANGE_NAME = "medici";
	private static final String CONSUMER_TAG = "medici_ply_obj";
	private static final boolean DURABLE = true;
	private static final boolean EXCLUSIVE = false;
	private static final boolean AUTO_DELETE = false;

	private static String serverAddr = "";
	private static String messageReceived = "";

	private static String meshlabserverPath = "";

	private static Log log = LogFactory.getLog(PlyObjExtractor.class);

	public static void main(String[] args)  {

		if (args.length < 4){
			System.out.println("Input RabbitMQ server address, followed by RabbitMQ username, RabbitMQ password and meshlabserver path.");
			return;
		}

		if(args[3].endsWith(System.getProperty("file.separator"))){
			args[3] = args[3].substring(0, args[3].length() - 1);
		}

		serverAddr = args[0];
		meshlabserverPath = args[3];
		new PlyObjExtractor().startExtractor(args[1], args[2]);	        
	}

	protected void startExtractor(String rabbitMQUsername, String rabbitMQpassword) {
		try{			 
			//Open channel and declare exchange and consumer
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(serverAddr);
			factory.setUsername(rabbitMQUsername);
			factory.setPassword(rabbitMQpassword);
			Connection connection = factory.newConnection();

			final Channel channel = connection.createChannel();
			channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);

			channel.queueDeclare(QUEUE_NAME,DURABLE,EXCLUSIVE,AUTO_DELETE,null);
			channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.file.model.ply.#");

			// create listener
			channel.basicConsume(QUEUE_NAME, false, CONSUMER_TAG, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					messageReceived = new String(body);
					long deliveryTag = envelope.getDeliveryTag();
					// (process the message components here ...)
					System.out.println(" {x} Received '" + messageReceived + "'");
					processMessageReceived();
					System.out.println(" [x] Done");
					channel.basicAck(deliveryTag, false);
				}
			});

			// start listening 
			System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
			while (true) {
				Thread.sleep(1000);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}		 
	}

	protected void processMessageReceived(){
		try{
			try {
				PlyObjExtractorService extrServ = new PlyObjExtractorService(meshlabserverPath);
				jobReceived = getRepresentation(messageReceived, ExtractionJob.class);
				File objFile = extrServ.processJob(jobReceived);

				log.info("Obj extraction complete. Returning obj file.");

				String objId = uploadPreview(objFile, "model/obj", log);

				String JSONString = mapper.writeValueAsString(new FileMetadata(objFile.getName(), new Long(objFile.length()).toString()));

				associatePreview(objId,JSONString, log);

				objFile.delete();

			} catch (IOException ioe) {
				log.error("The message could not be parsed into an ExtractionJob", ioe);
			}        
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
}
