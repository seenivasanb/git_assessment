# PlyObjExtractor

## Overview
Converts a .ply file to a .obj file

## Input
A .ply file to convert

## Output
The generated .obj file from conversion

## Dependencies
* ###Libraries
  
    * **MeshLab**  
   [http://meshlab.sourceforge.net/](http://meshlab.sourceforge.net/)

        $ sudo apt-get install meshlab