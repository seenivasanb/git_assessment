package plyobjextractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;

public class PlyObjExtractorService extends ExtractorService {

	private static Log log = LogFactory.getLog(PlyObjExtractorService.class);
	private static final String DOWNLOAD_KEY = "letmein";
	private static String meshlabserverPath = null;
	
	public PlyObjExtractorService(String meshlabserverPath){
		PlyObjExtractorService.meshlabserverPath = meshlabserverPath;
	}
	
	public File processJob(ExtractionJob receivedMsg) throws Exception{
		log.info("Downloading ply file with ID "+ receivedMsg.getIntermediateId() +" from " + receivedMsg.getHost());
						
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getId()+"?key="+DOWNLOAD_KEY);		
		HttpResponse fileResponse = httpclient.execute(httpGet);
		System.out.println(fileResponse.getStatusLine());
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9);
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")), fileName.substring(fileName.lastIndexOf(".")));
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();
		
		EntityUtils.consume(fileEntity);
		

		log.info("Download complete. Initiating obj extraction");

		File htmlFile = processFile(tempFile);	
		tempFile.delete();
		return htmlFile;				
	}
	
private File processFile(File tempFile) throws FileNotFoundException, IOException, InterruptedException  {
		
		Runtime r = Runtime.getRuntime();
		Process p;     // Process tracks one external native process
	    BufferedReader is;  // reader for output of process
	    String line;
	    
	    String tempDir = System.getProperty("java.io.tmpdir");
	    if (new Character(tempDir.charAt(tempDir.length()-1)).toString().equals(System.getProperty("file.separator")) == false){
	    	tempDir = tempDir + System.getProperty("file.separator");
	    }
	    
	    String cmd = meshlabserverPath + System.getProperty("file.separator") + "meshlabserver -i " + tempDir +  tempFile.getName() + " -o " 
				+ tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")) + ".obj";	    
		p = r.exec(cmd);
		
		is = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
	    while ((line = is.readLine()) != null)
	    	log.info(line);
	    p.waitFor();

		File outFile = new File(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")) + ".obj");
		return outFile;
	}
}
