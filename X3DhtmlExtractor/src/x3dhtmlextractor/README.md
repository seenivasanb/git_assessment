# X3DHtmlExtractor

## Overview
Generates an HTML preview of a given .x3d model.

## Input
An .x3d model to convert to an HTML preview.

## Output
An HTML preview of the input .x3d model.

## Dependencies
* ###Software
  
    * **InstantRealityPlayer**  
    [http://www.instantreality.org/downloads/](http://www.instantreality.org/downloads/)  
    Download the appropriate .deb file from the site above, then install:
    
        $ sudo dpkg -i [downloaded .deb package name]