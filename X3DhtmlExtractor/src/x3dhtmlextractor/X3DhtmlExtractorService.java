package x3dhtmlextractor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;

/**
 * @author Constantinos Sophocleous
 */
public class X3DhtmlExtractorService extends ExtractorService {

	private static Log log = LogFactory.getLog(X3DhtmlExtractorService.class);
	
	private ExtractionJob job = null;
	
	public X3DhtmlExtractorService(X3DhtmlExtractor x3DhtmlExtractor) {
		super(x3DhtmlExtractor);
	}

	public File processJob(ExtractionJob receivedMsg) throws Exception{
		log.info("Downloading x3d file with ID "+ receivedMsg.getIntermediateId() +" from " + receivedMsg.getHost());
		callingExtractor.sendStatus(receivedMsg.getId(), callingExtractor.getClass().getSimpleName(), "Downloading x3d file.", log);
		
		job = receivedMsg;
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getIntermediateId()+"?key="+playserverKey);		
		HttpResponse fileResponse = httpclient.execute(httpGet);
		log.info(fileResponse.getStatusLine());
		String statusString = fileResponse.getStatusLine().toString();
		if(statusString.indexOf("200") == -1){
			if(statusString.indexOf("500") >= 0 || statusString.indexOf("503") >= 0){
				throw new IOException("Server unable to process request.");
			}
			else
				throw new IOException("File not found.");
		}
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9).replace(" ", "-");
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")), fileName.substring(fileName.lastIndexOf(".")).toLowerCase());
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();
		
		EntityUtils.consume(fileEntity);
		

		log.info("Download complete. Initiating html extraction");

		File htmlFile = processFile(tempFile, receivedMsg.getId());	
		tempFile.delete();
		return htmlFile;				
	}

	private File processFile(File tempFile, String originalFileId) throws Exception  {
		
		Runtime r = Runtime.getRuntime();
		Process p;     // Process tracks one external native process
	    String line;
	    
	    String tempDir = System.getProperty("java.io.tmpdir");
	    if (new Character(tempDir.charAt(tempDir.length()-1)).toString().equals(System.getProperty("file.separator")) == false){
	    	tempDir = tempDir + System.getProperty("file.separator");
	    }
	    
	    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Converting to X3DOM HTML.", log);
	    
	    String cmd =  "aopt -i " + tempDir +  tempFile.getName() + " -N " 
				+ tempDir.substring(tempDir.indexOf(System.getProperty("file.separator")))
						+ tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")) + ".html";	    
		p = r.exec(cmd);
		StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
	    StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
	    outputGobbler.start();
	    errorGobbler.start();
	    p.waitFor();
	    File inFile = new File(tempDir + tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")) + ".html");
	    if(!Files.exists(inFile.toPath())){
	    	tempFile.delete();
	    	throw new Exception("File not processed correctly. File is possibly corrupt.");
	    }
						
	    File outFile = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".html");
	    BufferedWriter fileWriter =  new BufferedWriter(new FileWriter(outFile));
	    BufferedReader htmlReader = new BufferedReader(new FileReader(inFile));
	    boolean startWrite = false;
	    boolean endWrite = false;
	    while ((line = htmlReader.readLine()) != null){
	    	line = line.replaceAll("'\"", "'").replaceAll("\"'", "'");
	    	line = line.replaceAll("<shape", "<transform scale='1 1 1' data-actualshape='true'><shape");
	    	line = line.replaceAll("</shape>", "</shape></transform>");
	    	int startFrom = 0;
	    	int writeTo = line.length();
	    	int tmp;
	    	if((tmp = line.toLowerCase().indexOf("<scene")) != -1){
	    		int endOfModelMax = job.getFlags().indexOf("+", job.getFlags().indexOf("modelMaxDimension_"));
	    		if(endOfModelMax < 0)
	    			endOfModelMax = job.getFlags().length();
	    		String modelMaxDimension = job.getFlags().substring(job.getFlags().indexOf("modelMaxDimension_") + 18, endOfModelMax).replace("__", ".");
	    		line = line.replace("<scene", "<scene  data-modelMaxDimension='" + modelMaxDimension  + "'");
	    		writeTo = line.length();
	    		startFrom = tmp;
	    		startWrite = true;
	    	}
	    	if((tmp = line.toLowerCase().indexOf("/scene>")) != -1){
	    		writeTo = tmp + 7;
	    		endWrite = true;
	    	}
	    	if(startWrite)
	    		fileWriter.write(line,startFrom,writeTo - startFrom);	    	
	    	if(endWrite)
	    		break;
	    }
	    	
		htmlReader.close();
		inFile.delete();
        fileWriter.close();		
		return outFile;
	}
	
	
}
