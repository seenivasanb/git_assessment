package oniextractor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;
import metadata.FileMetadata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class OniExtractor extends Extractor {

	private static final String QUEUE_NAME = "medici_oni";
	private static final String CONSUMER_TAG = "medici_oni";
	private static final boolean DURABLE = true;
	private static final boolean EXCLUSIVE = false;
	private static final boolean AUTO_DELETE = false;

	private static String serverAddr = "";
	private static String messageReceived = "";

	private static Log log = LogFactory.getLog(OniExtractor.class);
	
	//10 MB
	private static Long decimationBound = Long.parseLong("10485760");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if (args.length < 6){
			System.out.println("Input RabbitMQ server address, followed by RabbitMQ username, RabbitMQ password, RabbitMQ exchange name" 
					+ " ,minimum size of any model to be decimated(in megabytes) and Medici REST API key.");
			return;
		}       
			serverAddr = args[0];
			decimationBound  = Long.parseLong(args[4]) * 1048576;
			OniExtractorService.setMaxSize(decimationBound);
			OniExtractorService.setPlayserverKey(args[5]);
			OniExtractor.setPlayserverKey(args[5]);
			OniExtractor.setExchangeName(args[3]);
			new OniExtractor().startExtractor(args[1], args[2]);
	}

	protected void startExtractor(String rabbitMQUsername,String rabbitMQpassword) {		
		try {
			//Open channel and declare exchange and consumer
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(serverAddr);
			factory.setUsername(rabbitMQUsername);
			factory.setPassword(rabbitMQpassword);
			Connection connection = factory.newConnection();
	    
			final Channel channel = connection.createChannel();
			channel.exchangeDeclare(exchange_name, "topic", true);
	
			channel.queueDeclare(QUEUE_NAME,DURABLE,EXCLUSIVE,AUTO_DELETE,null);
			channel.queueBind(QUEUE_NAME, exchange_name, "*.file.application.oni.#");
			
			this.channel = channel;
							
			 // create listener
	        channel.basicConsume(QUEUE_NAME, false, CONSUMER_TAG, new DefaultConsumer(channel) {
	            @Override
	            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
	                messageReceived = new String(body);
	                long deliveryTag = envelope.getDeliveryTag();
	                // (process the message components here ...)
	                
	                replyProps = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();
					replyTo = properties.getReplyTo();
	                
	                System.out.println(" {x} Received '" + messageReceived + "'");
		            
	                if(processMessageReceived()){
			            System.out.println(" [x] Done");
		                channel.basicAck(deliveryTag, false);
	                }
		            else{
		            	System.out.println(" [x] Error processing");
		            	channel.basicAck(deliveryTag, false);
		            }
	            }
	        });
			
	        // start listening 
	        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
	        while (true) {
	           Thread.sleep(1000);
	        }
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	protected boolean processMessageReceived() {
		try {
			try {
				OniExtractorService extractorService = new OniExtractorService(this);
				jobReceived = getRepresentation(messageReceived, ExtractionJob.class);
				
				if(jobReceived.isFlagSet("nopreviews")){
					log.info("oni is not to be previewed. Aborted processing of oni.");
    				return true;
				}
								
				File previewFile = File.createTempFile("oni_preview", ".oniv");
				BufferedWriter previewFileWriter = new BufferedWriter(new FileWriter(previewFile));
				File[] htmlFiles = extractorService.processJob(jobReceived);
				if(htmlFiles.length == 0){
					previewFile.delete();
					throw new Exception("File not processed correctly. File is possibly corrupt.");
				}
				
				StringBuilder previewPaths = new StringBuilder();

    			log.info("ONI/PLY frames extraction complete. Returning frame files.");
    			sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "ONI/PLY frames extraction complete. Returning frame files.", log);
    			
    			for(int i=0; i < htmlFiles.length; i++)
    			{
	    			String htmlFileId = uploadPreview(htmlFiles[i], "text/html", log);
	    			//String JSONString = mapper.writeValueAsString(new FileMetadata(htmlFiles[i].getName(), new Long(htmlFiles[i].length()).toString()));
	    			htmlFiles[i].delete();
	    			previewPaths.append(htmlFileId + ",");
    			}
    			htmlFiles[0].getParentFile().delete();
    			
    			previewFileWriter.write(previewPaths.toString());
    			previewFileWriter.close();
    			String previewFileId = uploadPreview(previewFile, "text/oni-view", log);	
    			String JSONString = mapper.writeValueAsString(new FileMetadata(previewFile.getName(), new Long(previewFile.length()).toString()));	
    			associatePreview(previewFileId,JSONString, log);
    			previewFile.delete();
    	        
    	        sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
    	        return true;

			} catch (Exception ioe) {
				log.error("Could not finish extraction job.", ioe);
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "Could not finish extraction job.", log);
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
			return false;
		}
	}

}
