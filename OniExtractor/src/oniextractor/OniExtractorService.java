package oniextractor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class OniExtractorService extends ExtractorService {
	
	//Maximum wait (in seconds) for meshlabserver to finish each workflow task per MB of size of 3D model file.
		private static final float CONVERSION_WAIT_PER_MB = 0.924f;
		private static final float CENTERING_WAIT_PER_MB = 2.58f;
		private static final float SCALING_WAIT_PER_MB = 1.74f;
		private static final float DECIMATION_WAIT_PER_MB = 6.48f;

	private static Log log = LogFactory.getLog(OniExtractorService.class);
	
	private ExtractionJob job = null;
	
	private Float modelMaxDimension = new Float(1.0);
	//private static Long maxFileSize = Long.parseLong("31457280"); // 30 MB
	//private static Long maxFileSize = Long.parseLong("5242880"); // 5 MB
	private static Long maxFileSize = Long.parseLong("10485760"); // 10 MB 
	
	public OniExtractorService(OniExtractor oniExtractor){
		super(oniExtractor);
	}
	
	public static void setMaxSize(Long maxSize) {
		OniExtractorService.maxFileSize = maxSize;
	}
	
	public File[] processJob(ExtractionJob receivedMsg) throws Exception{
		
		log.info("Downloading oni file with ID "
				+ receivedMsg.getIntermediateId() + " from "
				+ receivedMsg.getHost());
		callingExtractor.sendStatus(receivedMsg.getId(), callingExtractor.getClass().getSimpleName(), "Downloading oni file.", log);

		job = receivedMsg;
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getIntermediateId()+"?key="+playserverKey);		
		HttpResponse fileResponse = httpclient.execute(httpGet);
		log.info(fileResponse.getStatusLine());
		if(fileResponse.getStatusLine().toString().indexOf("200") == -1){
			throw new IOException("File not found.");
		}
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9);
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")), fileName.substring(fileName.lastIndexOf(".")).toLowerCase());
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();
		
		EntityUtils.consume(fileEntity);
		
		log.info("Download complete. Initiating 3d model frames extraction.");
		
		File[] frameFiles = processFile(tempFile, receivedMsg.getId());
		return frameFiles;	
	}
	
	private File[] processFile(File oniFile, String originalFileId) throws Exception  {		
				
		/*Here the oni file is processed, first converted into a list of pcd files and later on to a list of ply files. 
		These files are uploaded as a preview of the oni file.*/ 
		
		Runtime r = Runtime.getRuntime();
		Process p;     // Process tracks one external native process
	    String line;
	    StreamGobbler outputGobbler;
	    StreamGobbler errorGobbler;
	    
	    String tempDir = System.getProperty("java.io.tmpdir");
	    if (new Character(tempDir.charAt(tempDir.length()-1)).toString().equals(System.getProperty("file.separator")) == false){
	    	tempDir = tempDir + System.getProperty("file.separator");
	    }
	    
	    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Extracting pcd files.", log);
	    
		File workingDir = Files.createTempDirectory(job.getId() + "_").toFile();
		
		//Convert oni file to pcd file frames  

		String cmd = "pcl_oni2pcd " + oniFile.getAbsolutePath();
		p = r.exec(cmd, null, workingDir);
		outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
	    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
	    outputGobbler.start();
	    errorGobbler.start();
		p.waitFor();
		
		callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Converting pcd files to PLYs.", log);
								
		oniFile.delete();
		//File [] pcdFiles = workingDir.listFiles();
		DirectoryStream<Path> dStream = Files.newDirectoryStream(workingDir.toPath());
		Iterator<Path> test = dStream.iterator();
		ArrayList<String> fileNamesList = new ArrayList<String>();
		ArrayList<File> outputFilesList = new ArrayList<File>();
		HashMap<String, File> fileMap = new HashMap<String, File>();
		String fileName;
		File file;
		while(test.hasNext())
		{
			file = test.next().toFile();
			fileName = file.getName();
			fileNamesList.add(fileName);
			fileMap.put(fileName, file);
		}
		Collections.sort(fileNamesList);
		fileNamesList.trimToSize();
		int length = fileNamesList.size();
		File scalingScript = null;
		File quadricScript = null;
		File centerScript = null; 
		
		
		//Convert each of those pcd files to a ply file
				for(int i=0; i<length; i++){
					try{
						//fileName = pcdFiles[i].getName();
						fileName = fileNamesList.get(i);
						cmd = "pcl_pcd2ply -use_camera 0 " + fileName + " " +fileName.replace(".pcd", ".ply");
						p = r.exec(cmd, null, workingDir);
						outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
					    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
					    outputGobbler.start();
					    errorGobbler.start();					
						p.waitFor();
						File delFile = fileMap.get(fileName);
						delFile.delete();
						
						//pcdFiles[i].delete();
						
						File tempFile = new File(workingDir, fileName.replace(".pcd", ".ply"));
						fileName = fileName.replace(".pcd", ".ply");
						///////////////////////////////////////////////////////
						
						log.info("Converting model to X3D.");
				    	//callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Converting model to X3D.", log);
				    	
				    	cmd = "meshlabserver -i " + fileName + " -o " 
								+ "x3d__" + fileName.replace(".ply", ".x3d")
								+ " -om vc vf vq vn wt";	    
						p = r.exec(cmd, null, workingDir);
						outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
					    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
					    outputGobbler.start();
					    errorGobbler.start();
					    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * CONVERSION_WAIT_PER_MB).intValue())){
					    	tempFile.delete();
					    	throw new Exception("File not processed correctly. File is possibly corrupt.");
					    }
					    tempFile.delete();
				    	tempFile = new File(workingDir, "x3d__" + fileName.replace(".ply", ".x3d"));
				    	fileName = tempFile.getName();
				    	
				    	/////////////////////////////////////////////////////////
				    	
				    	log.info("Centering model to center of bounding box.");
					    //callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Centering model to center of bounding box.", log);
				
					    //Create centering script.
				    	if(centerScript == null)
				    		centerScript = createCenteringScriptFile(tempFile);
				
					    cmd = "meshlabserver -i " + fileName + " -o " 
					    		+ "center__" + fileName + " -s " + tempDir + centerScript.getName()
					    		+ " -om vc vf vq vn wt";	    
					    p = r.exec(cmd, null, workingDir);
					    outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
					    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
					    outputGobbler.start();
					    errorGobbler.start();
					    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * CENTERING_WAIT_PER_MB).intValue())){
					    	tempFile.delete();
					    	throw new Exception("File not processed correctly. File is possibly corrupt.");
					    }
					    tempFile.delete();		    
					    tempFile = new File(workingDir, "center__" + fileName);		    
					    fileName = tempFile.getName(); 		    
				    	
						///////////////////////////////////////////////////////////////////
							    
					    log.info("Measuring model.");
					    //callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Measuring model.", log);
					    
					    try{
						    modelMaxDimension = getMaxDimension(tempFile);}
						    catch(Exception e){
						    	tempFile.delete();
						    	throw e;
						    }
					    
					    log.info("Max dimension of model is " +  new DecimalFormat("#.####").format(modelMaxDimension));
					    
					    ///////////////////////////////////////////////////////////////////
					    
					    log.info("Scaling model.");
					    //callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Scaling model.", log);
						
					    //Create scaling script.
					    if(scalingScript == null)
					    	scalingScript = createScalingScriptFile(tempFile);
				
					    cmd = "meshlabserver -i " + fileName + " -o " 
					    		+ "scaled__" + fileName + " -s " + tempDir + scalingScript.getName()
					    		+ " -om vc vf vq vn wt";	    
					    p = r.exec(cmd, null, workingDir);
					    outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
					    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
					    outputGobbler.start();
					    errorGobbler.start();
					    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * SCALING_WAIT_PER_MB).intValue())){
					    	tempFile.delete();
					    	throw new Exception("File not processed correctly. File is possibly corrupt.");
					    }
				
					    tempFile.delete();		    
					    tempFile = new File(workingDir, "scaled__" + tempFile.getName());
					    fileName = tempFile.getName();
					    
					    ///////////////////////////////////////////////////////////////////
					    
					    if(tempFile.length() > maxFileSize){
					    	log.info("Decimating model.");
					    	//callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Decimating model.", log);
					    	
					    	//Create quadric collapse decimation script.		    		
					    	quadricScript = createQuadricScriptFile(tempFile);
				
					    	cmd = "meshlabserver -i " + fileName + " -o " 
					    			+ "dec__" + fileName + " -s " + tempDir + quadricScript.getName()
					    			+ " -om vc vf vq vn wt";	    
					    	p = r.exec(cmd, null, workingDir);
					    	outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
						    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
						    outputGobbler.start();
						    errorGobbler.start();
						    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * DECIMATION_WAIT_PER_MB).intValue())){
						    	quadricScript.delete();
						    	tempFile.delete();
						    	throw new Exception("File not processed correctly. File is possibly corrupt.");
						    }
						    
						    quadricScript.delete();
					    	tempFile.delete();		    	
					    	tempFile = new File(workingDir, "dec__" + fileName);
					    	fileName = tempFile.getName();
					    }
					    
					    //callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Converting to X3DOM HTML.", log);
					    
					    cmd =  "aopt -i " + fileName + " -N " 
								+ fileName.replace(".x3d", ".html");	    
						p = r.exec(cmd, null, workingDir);
						outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
					    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
					    outputGobbler.start();
					    errorGobbler.start();
					    p.waitFor();
					    
					    tempFile.delete();
					    tempFile = new File(workingDir, fileName.replace(".x3d", ".html"));
					    
						if(!Files.exists(tempFile.toPath())){
							throw new Exception("File not processed correctly. File is possibly corrupt.");
						}
					    
					    File outFile = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".html", workingDir);
					    BufferedWriter fileWriter =  new BufferedWriter(new FileWriter(outFile));
					    BufferedReader htmlReader = new BufferedReader(new FileReader(tempFile));
					    boolean startWrite = false;
					    boolean endWrite = false;
					    while ((line = htmlReader.readLine()) != null){
					    	line = line.replaceAll("'\"", "'").replaceAll("\"'", "'");
					    	line = line.replaceAll("<shape", "<transform scale='1 1 1' data-actualshape='true'><shape");
					    	line = line.replaceAll("</shape>", "</shape></transform>");
					    	int startFrom = 0;
					    	int writeTo = line.length();
					    	int tmp;
					    	if((tmp = line.toLowerCase().indexOf("<scene")) != -1){
					    		int endOfModelMax = job.getFlags().indexOf("+", job.getFlags().indexOf("modelMaxDimension_"));
					    		if(endOfModelMax < 0)
					    			endOfModelMax = job.getFlags().length();
					    		//String modelMaxDimension = job.getFlags().substring(job.getFlags().indexOf("modelMaxDimension_") + 18, endOfModelMax).replace("__", ".");
					    		//String modelMaxDimension = modelMaxDimension; 
					    		line = line.replace("<scene", "<scene  data-modelMaxDimension='" + modelMaxDimension  + "'");
					    		writeTo = line.length();
					    		startFrom = tmp;
					    		startWrite = true;
					    	}
					    	if((tmp = line.toLowerCase().indexOf("/scene>")) != -1){
					    		writeTo = tmp + 7;
					    		endWrite = true;
					    	}
					    	if(startWrite)
					    		fileWriter.write(line,startFrom,writeTo - startFrom);	    	
					    	if(endWrite)
					    		break;
					    }
					    
					    outputFilesList.add(outFile);
					    
					    htmlReader.close();
						tempFile.delete();
				        fileWriter.close();		
						//return outFile;
					}catch(Exception e){
						log.error(e);
					}
				}
				
				/*cmd = "rm *.pcd";
				p = r.exec(cmd, null, workingDir);
				p.waitFor();
				is.close();*/
				if(centerScript != null)
					centerScript.delete();
				if(scalingScript != null)
					scalingScript.delete();
				
				//System.exit(0);
							
				//Creating the zip file
				//File [] htmlFiles = workingDir.listFiles();
				outputFilesList.trimToSize();
				File [] htmlFiles = new File[outputFilesList.size()];
				File [] htmlFilesOut  = outputFilesList.toArray(htmlFiles);
				/*for(int i=0; i< htmlFiles.length; i++){
										
		            FileInputStream fin = new FileInputStream(htmlFiles[i]);
		            zOs.putNextEntry(new ZipEntry(htmlFiles[i].getName()));
		            
		            while((length = fin.read(buffer)) > 0)
		            {
		               zOs.write(buffer, 0, length);
		            }
		            fin.close();
		            htmlFiles[i].delete();
				}*/
				//zOs.close();		
				return htmlFiles;
			}
			
			private Float getMaxDimension(File tempFile) throws IOException {
				
				Float maxX = new Float(-new Float(1000000)), minX = new Float(1000000), maxY = new Float(-new Float(1000000)), minY = new Float(1000000), maxZ = new Float(-new Float(1000000)), minZ = new Float(1000000);
				
				BufferedReader fileReader = new BufferedReader(new FileReader(tempFile));
				String line = null;
				while ((line = fileReader.readLine()) != null){
					if(line.toLowerCase().indexOf("<coordinate") >= 0){
						line = line.replace('\"', '\'');
						String[] coords = line.substring(line.indexOf("point=")+7,  line.indexOf("\'",line.indexOf("point=")+7)).trim().split(" +");
						line = null;
						for (int i = 0; i < coords.length; i+= 3){
							Float currX = new Float(coords[i]), currY = new Float(coords[i+1]), currZ = new Float(coords[i+2]);					
							if(currX > maxX)
								maxX = currX;
							if(currX < minX)
								minX = currX;
							if(currY > maxY)
								maxY = currY;
							if(currY < minY)
								minY = currY;
							if(currZ > maxZ)
								maxZ = currZ;
							if(currZ < minZ)
								minZ = currZ;
						}
						coords = null;
					}
					line = null;
				}
				fileReader.close();
				
				Float maxDimension = Math.max(Math.max(maxX - minX,maxY - minY), maxZ - minZ);
				
				return maxDimension;
			}

			private File createCenteringScriptFile(File tempFile) throws IOException {
				
				File centerScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(centerScript));
				fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Transform: Move, Translate, Center\">"
								+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisX\" max=\"293.67\"/>"
								+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisY\" max=\"293.67\"/>"
								+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisZ\" max=\"293.67\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"centerFlag\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"Freeze\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"ToAll\"/>"
								+ "</filter></FilterScript>");
				fileWriter.close();
				
				return centerScript;
			}
			
			private File createScalingScriptFile(File tempFile) throws IOException {
				
				File scalingScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(scalingScript));
				fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Transform: Scale\">"
								+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisX\" max=\"10\"/>"
								+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisY\" max=\"10\"/>"
								+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisZ\" max=\"10\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"uniformFlag\"/>"
								+ "<Param enum_val0=\"origin\" enum_val1=\"barycenter\" enum_cardinality=\"3\" enum_val2=\"custom point\" type=\"RichEnum\" value=\"0\" name=\"scaleCenter\"/>"
								+ "<Param x=\"0\" y=\"0\" z=\"0\" type=\"RichPoint3f\" name=\"customCenter\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"unitFlag\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"Freeze\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"ToAll\"/>"
								+ "</filter></FilterScript>");
				fileWriter.close();
				
				return scalingScript;
			}
			
			private File createQuadricScriptFile(File tempFile) throws IOException {
				
				File quadricScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(quadricScript));
				fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Quadric Edge Collapse Decimation\">"
								+ "<Param type=\"RichInt\" value=\"1274379378\" name=\"TargetFaceNum\"/>"
								+ "<Param type=\"RichFloat\" value=\""
								+ new DecimalFormat("#.##").format(new Double(Double.parseDouble(maxFileSize.toString())/ Double.parseDouble(new Long(tempFile.length()).toString()))) 
								+ "\" name=\"TargetPerc\"/>"
								+ "<Param type=\"RichFloat\" value=\"1\" name=\"QualityThr\"/>"
								+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveBoundary\"/>"
								+ "<Param type=\"RichFloat\" value=\"1\" name=\"BoundaryWeight\"/>"
								+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveNormal\"/>"
								+ "<Param type=\"RichBool\" value=\"true\" name=\"PreserveTopology\"/>"
								+ "<Param type=\"RichBool\" value=\"false\" name=\"OptimalPlacement\"/>"
								+ "<Param type=\"RichBool\" value=\"false\" name=\"PlanarQuadric\"/>"
								+ "<Param type=\"RichBool\" value=\"false\" name=\"QualityWeight\"/>"
								+ "<Param type=\"RichBool\" value=\"false\" name=\"AutoClean\"/>"
								+ "<Param type=\"RichBool\" value=\"false\" name=\"Selected\"/>"
								+ "</filter></FilterScript>");
				fileWriter.close();
				
				return quadricScript;
			}
			
			
			private boolean customWaitFor(Process p, int seconds){
				
				long now = System.currentTimeMillis();
				long timeoutInMillis = 1000L * seconds; 
			    long finish = now + timeoutInMillis + 30000;
			    try{
				    while ( isAlive( p ) )
				    { 
				        Thread.sleep( 10 ); 
				        if ( System.currentTimeMillis() > finish ) {
				            p.destroy();
				            return false;
				        }
				    }
				    return true;
			    }
			    catch (Exception err) {
			      err.printStackTrace();
			      return false;
			        }
			}
			public static boolean isAlive( Process p ) {  
			    try  
			    {  
			        p.exitValue();  
			        return false;  
			    } catch (IllegalThreadStateException e) {  
			        return true;  
			    } 
			}
}