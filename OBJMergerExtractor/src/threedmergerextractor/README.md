# ThreeDMergerExtractor

## Overview
Unzips a file containing a single .obj or .x3d model to be processed.  If necessary the model is converted into .x3d format.  The model is centered to the center of the bounding box and scaled.
If the model is greater than the extractor service's maximum file size (default 30MB), the model is decimated.  The model's texture file references are flattened, the texture files are uploaded and associated with the original model, and the .x3d model is returned.
Very large models are also pre-decimated to facilitate processing.

## Input
A zip file containing a single .obj or .x3d model to be processed.  The zip can contain an arbitrary number of other files (texture files, etc), but should only have one .obj or .x3d file to process.

## Output
Texture files are uploaded and the centered, scaled, and decimated .x3b model is returned.


## Dependencies
* ###Software
      
    * **MeshLab**  
   [http://meshlab.sourceforge.net/](http://meshlab.sourceforge.net/)

        $ sudo apt-get install meshlab   