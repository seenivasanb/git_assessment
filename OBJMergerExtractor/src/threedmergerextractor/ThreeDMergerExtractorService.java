package threedmergerextractor;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;
import metadata.FileMetadata;

/**
 * @author Constantinos Sophocleous
 */
public class ThreeDMergerExtractorService extends ExtractorService {
	
	//Maximum wait (in seconds) for meshlabserver to finish each workflow task per MB of size of 3D model file.
		private static final float CONVERSION_WAIT_PER_MB = 10.0f;
		private static final float CENTERING_WAIT_PER_MB = 10.0f;
		private static final float SCALING_WAIT_PER_MB = 10.0f;
		private static final float DECIMATION_WAIT_PER_MB = 10.0f;

	private static Log log = LogFactory.getLog(ThreeDMergerExtractorService.class);
	private static final String[] mtlTextureMapFormats = new String[] {".jpg",".jpeg",".jpe",".jif","jfif",".jfi",
		   ".jp2",".j2k",".jpf",".jpx",".jpm",".mj2",".tiff",".tif",".gif",".bmp",".dib",".png",".tga"};
	private static final String[] mtlImageTextureMapFormats = new String[] {".jpg",".jpeg",".jpe",".jif","jfif",".jfi",
		   ".jp2",".j2k",".jpf",".jpx",".jpm",".mj2",".tiff",".tif",".gif",".bmp",".dib",".png",".tga"};
	
	//30 MB
	private static Long maxTotalSize = Long.parseLong("31457280");	
	//1200 MB
	private static Long predecimationMaxFileSize = Long.parseLong("1258291200");
	
	private Float modelMaxDimension = new Float(1.0);
	
	private static int uploadThreads = Runtime.getRuntime().availableProcessors();
	private static final long EXEC_MAX_WAIT = 604800;
	
	private ExtractionJob job = null;
	
	
	private String modelType = "x3d";
		
	public ThreeDMergerExtractorService(
			ThreeDMergerExtractor threeDMergerExtractor) {
		super(threeDMergerExtractor);
	}
	
	public static void setMaxSize(Long maxSize) {
		ThreeDMergerExtractorService.maxTotalSize = maxSize;
	}
	
	public static void setMaxPreSize(Long maxPreSize) {
		ThreeDMergerExtractorService.predecimationMaxFileSize = maxPreSize;
	}
	
	public static void setUploadThreads(Integer uploadThreads) {
		ThreeDMergerExtractorService.uploadThreads = uploadThreads;
	}
	
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	
	public Float getModelMaxDimension(){
		return modelMaxDimension;
	}
	
	public File processJob(ExtractionJob receivedMsg) throws Exception{
		
		job = receivedMsg;
		
		log.info("Downloading zipped " + modelType + " files in ZIP file with ID "+ receivedMsg.getIntermediateId() +" from " + receivedMsg.getHost());
		callingExtractor.sendStatus(receivedMsg.getId(), callingExtractor.getClass().getSimpleName(), "Downloading zipped model file.", log);
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getIntermediateId()+"?key="+playserverKey);		
		HttpResponse fileResponse = httpclient.execute(httpGet);
		log.info(fileResponse.getStatusLine());
		String statusString = fileResponse.getStatusLine().toString();
		if(statusString.indexOf("200") == -1){
			if(statusString.indexOf("500") >= 0 || statusString.indexOf("503") >= 0){
				throw new IOException("Server unable to process request.");
			}
			else
				throw new IOException("File not found.");
		}
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9).replace(" ", "-");
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")).replace(" ", "_"), fileName.substring(fileName.lastIndexOf(".")).toLowerCase());
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();
		
		EntityUtils.consume(fileEntity);
		

		log.info("Download complete. Initiating " + modelType + " merging");

		File objMergedFile = processFile(tempFile, receivedMsg.getId());	
		return objMergedFile;				
	}

	private File processFile(File tempFile, String originalFileId) throws Exception {
		
		Runtime r = Runtime.getRuntime();
		Process p;     // Process tracks one external native process
	    String line;
	    String cmd;
	    StreamGobbler outputGobbler;
	    StreamGobbler errorGobbler;

	    byte[] buffer = new byte[1024];
		
	    log.info("Unzipping ZIP file");
	    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Unzipping ZIP file.", log);
	    
	    String tempDir = System.getProperty("java.io.tmpdir");
	    if (new Character(tempDir.charAt(tempDir.length()-1)).toString().equals(System.getProperty("file.separator")) == false){
	    	tempDir = tempDir + System.getProperty("file.separator");
	    }
	    
	    Path unzipDir = Files.createTempDirectory(job.getId() + "_");
	    File unzipDirFile = unzipDir.toFile();
	    
	    ZipInputStream zis = null;
	    try{
		    zis = new ZipInputStream(new FileInputStream(tempFile));
		    ZipEntry ze = zis.getNextEntry();
		    
		    while(ze!=null){
		    	if(! ze.isDirectory()){	
	
		    		String fileName = ze.getName();
		    		if(fileName.endsWith(".obj"))
		    			fileName = fileName.replace(" ", "-");
		    		
		    		File newFile = new File(tempDir + unzipDirFile.getName() + System.getProperty("file.separator") + fileName);
	
		    		log.info("File unzip : "+ newFile.getCanonicalPath());
	
		    		new File(newFile.getParent()).mkdirs();
	
		    		FileOutputStream fos = new FileOutputStream(newFile);             
	
		    		int len;
		    		while ((len = zis.read(buffer)) > 0) {
		    			fos.write(buffer, 0, len);
		    		}
	
		    		fos.close();
		    	}   
		    	ze = zis.getNextEntry();
		    }
	    	
	    	zis.closeEntry();
	    	zis.close();
	    }catch(Exception e){
	    	try{
		    	if(zis != null)
		    		zis.close();
		    	tempFile.delete();
		    	FileUtils.deleteDirectory(unzipDirFile);
	    	}
	    	catch(Exception e2){
	    		log.error(e2);
	    	}
	    	finally{
	    		throw new Exception("ZIP file not processed correctly. ZIP file is possibly corrupt.");
	    	}
	    }
 
    	tempFile.delete();
    	
    	log.info("Done unzipping");
    	
	    ///////////////////////
    	
    	ArrayList<File> objFiles = new ArrayList<File>();
    	searchForFilesWithExtensions(unzipDirFile, objFiles, new String[] {"." + modelType});
    	
    	if(objFiles.size() == 0){
    		log.error("ERROR: No " + modelType + " file found. Aborting processing.");
    	    FileUtils.deleteDirectory(unzipDir.toFile());
    	    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "ERROR: No " + modelType + " file found. Aborting processing.", log);
    	    return null;
    	}
    	if(objFiles.size() > 1){
    		log.error("ERROR: More than one " + modelType + " files found. Aborting processing.");
    	    FileUtils.deleteDirectory(unzipDir.toFile());
    	    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "ERROR: More than one " + modelType + " files found. Aborting processing.", log);
    	    return null;
    	}
	    	
    	String fileName = tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")) + ".x3d";
    	String filePath = "";
    	boolean deleteTemp;
    	
	    ///////////////////////
    	File outFile = null;
    	try{
	    	if(!modelType.equals("x3d") || objFiles.get(0).length() > predecimationMaxFileSize){
	    		log.info("Converting model to X3D and predecimating if needed.");  
	    		callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Converting model to X3D and predecimating if needed.", log);
	    		String predecimationCommand = "";
		    	File quadricScriptPre = null;
		    	if(objFiles.get(0).length() > predecimationMaxFileSize){
		    		Double decimationProportionPre = new Double(Double.parseDouble(predecimationMaxFileSize.toString())/ Double.parseDouble(new Long(objFiles.get(0).length()).toString()));
		    		//Create quadric collapse decimation script.
			    	quadricScriptPre = createQuadricScriptFile(tempFile, decimationProportionPre);
			    	predecimationCommand = " -s " + tempDir + quadricScriptPre.getName();
		    	}
	    		
	    		cmd = "meshlabserver -i " + objFiles.get(0).getPath() + " -o " 
	    				+ tempDir + fileName
	    				+ predecimationCommand
	    				+ " -om vc vf vq vn wt";
	    		p = r.exec(cmd, null, objFiles.get(0).getParentFile());
	    		outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
	    	    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
	    	    outputGobbler.start();
	    	    errorGobbler.start();	    	    
	    		if(!customWaitFor(p, new Double(Double.parseDouble(new Long(objFiles.get(0).length()).toString())/1024/1024 * CONVERSION_WAIT_PER_MB).intValue())){
	    			if(quadricScriptPre != null)
			    		quadricScriptPre.delete();
			    	throw new Exception("File not processed correctly. File is possibly corrupt.");
			    }
	
	    		tempFile = new File(tempDir + fileName);
	    		filePath = tempDir + fileName;
	    		
	    		deleteTemp = true;
		    }
	    	else{
	    		filePath = objFiles.get(0).getPath();
	    		tempFile = objFiles.get(0);
	    		
	    		deleteTemp = false;
	    	}
		    
		    ///////////////////////
	    	
		    log.info("Centering model to center of bounding box.");
		    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Centering model to center of bounding box.", log);
	
		    //Create centering script.
		    File centerScript = createCenteringScriptFile(tempFile);
	
		    cmd = "meshlabserver -i " + filePath + " -o " 
		    		+ tempDir + "center__" + fileName + " -s " + tempDir + centerScript.getName()
		    		+ " -om vc vf vq vn wt";	    
		    p = r.exec(cmd, null, objFiles.get(0).getParentFile());
		    outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
		    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
		    outputGobbler.start();
		    errorGobbler.start();
		    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * CENTERING_WAIT_PER_MB).intValue())){
		    	centerScript.delete();
		    	if(deleteTemp && Files.exists(tempFile.toPath()))
			    	tempFile.delete();
		    	throw new Exception("File not processed correctly. File is possibly corrupt.");
		    }
	
		    centerScript.delete();
		    if(deleteTemp)
		    	tempFile.delete();	    
		    
		    tempFile = new File(tempDir + "center__" + fileName);
		    
		    ///////////////////////
		    
		    log.info("Measuring model.");
		    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Measuring model.", log);
		    
		    try{
			    modelMaxDimension = getMaxDimension(tempFile);}
			    catch(Exception e){
			    	if(Files.exists(tempFile.toPath()))
			    		tempFile.delete();
			    	throw e;
			    }
		    
		    log.info("Max dimension of model is " +  new DecimalFormat("#.####").format(modelMaxDimension));
		    
		    ///////////////////////
		    
		    log.info("Scaling model.");
		    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Scaling model.", log);
			
		    //Create scaling script.
		    File scalingScript = createScalingScriptFile(tempFile);
	
		    cmd = "meshlabserver -i " + tempDir +  tempFile.getName() + " -o " 
		    		+ tempDir + "scaled__" + tempFile.getName() + " -s " + tempDir + scalingScript.getName()
		    		+ " -om vc vf vq vn wt";	    
		    p = r.exec(cmd, null, objFiles.get(0).getParentFile());
		    outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
		    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
		    outputGobbler.start();
		    errorGobbler.start();
		    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * SCALING_WAIT_PER_MB).intValue())){
		    	scalingScript.delete();
		    	if(Files.exists(tempFile.toPath()))
			    	tempFile.delete();
		    	throw new Exception("File not processed correctly. File is possibly corrupt.");
		    }
	
		    File scaledFile = new File(tempDir + "scaled__" + tempFile.getName());
		    scalingScript.delete();
		    tempFile.delete();	    
		    tempFile = scaledFile;
		    scaledFile = null;
		    
		    ///////////////////////
		    
		    ArrayList<File> imageTextureFiles = new ArrayList<File>();	    
			searchForFilesWithExtensions(unzipDirFile, imageTextureFiles, mtlImageTextureMapFormats);
		 	long totalSize = tempFile.length();
		 	for(int i = 0; i < imageTextureFiles.size(); i++)
		 		totalSize = totalSize + imageTextureFiles.get(i).length();
		 	
		 	if(totalSize > maxTotalSize){
		 		log.info("Decimating model.");
		 		callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Decimating model.", log);
	
		 		Double decimationProportion = new Double(Double.parseDouble(maxTotalSize.toString())/ Double.parseDouble(new Long(totalSize).toString()));
	
		 		//Create quadric collapse decimation script.
		 		File quadricScript = createQuadricScriptFile(tempFile, decimationProportion);
	
		 		cmd = "meshlabserver -i " + tempDir +  tempFile.getName() + " -o " 
		 				+ tempDir + "dec__" + tempFile.getName() + " -s " + tempDir + quadricScript.getName()
		 				+ " -om vc vf vq vn wt";	    
		 		p = r.exec(cmd, null, objFiles.get(0).getParentFile());
		 		outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
			    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
			    outputGobbler.start();
			    errorGobbler.start();
			    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * DECIMATION_WAIT_PER_MB).intValue())){
			    	quadricScript.delete();
			    	if(Files.exists(tempFile.toPath()))
				    	tempFile.delete();
			    	throw new Exception("File not processed correctly. File is possibly corrupt.");
			    }
	
			    quadricScript.delete();
		 		tempFile.delete();		 		
	
		 		tempFile = new File(tempDir + "dec__" + tempFile.getName());
	
	
		 		log.info("Decimating image textures.");
		 		callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Decimating image textures.", log);
		 		for(int i = 0; i < imageTextureFiles.size(); i++){
		 			FileInputStream fis = null;
		 			try{
			 			fis = new FileInputStream(imageTextureFiles.get(i));
			 			BufferedImage image = ImageIO.read(fis);
			 			fis.close();
			 			BufferedImage decImage = getScaledImage(image, decimationProportion);
			 			ImageIO.write(decImage, imageTextureFiles.get(i).getName().substring(imageTextureFiles.get(i).getName().lastIndexOf(".")+1), imageTextureFiles.get(i));
		 			}catch(Exception e){
		 				fis.close();
		 			}
		 		}
		 	}
		 	
		    ///////////////////////
			
		    log.info("Flattening texture files references.");
		    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Flattening texture files references.", log);
	 	    outFile = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".x3d");
	 	    BufferedWriter fileWriter =  new BufferedWriter(new FileWriter(outFile));
	 	    BufferedReader x3dReader = new BufferedReader(new FileReader(tempFile));
	 	    String objFileDirFlattenedPath = objFiles.get(0).getPath().replace(tempDir + unzipDirFile.getName()  + System.getProperty("file.separator") , "");
	 	    objFileDirFlattenedPath = objFileDirFlattenedPath.substring(0, objFileDirFlattenedPath.lastIndexOf(System.getProperty("file.separator")) + 1);
	 	    objFileDirFlattenedPath = objFileDirFlattenedPath.replace(System.getProperty("file.separator"), "/");
	 	    try{
		 	    while ((line = x3dReader.readLine()) != null){
		 	    	if((line.toLowerCase().indexOf("<imagetexture")) != -1){
		 	    		line = line.replace("'", "\"");
		 	    		
		 	    		String textureUrl = line.substring(line.indexOf("url=\"") + 5, line.indexOf("\"", line.indexOf("url=\"") + 5));
		 	    		String textureUrlOld = textureUrl;
		 	    		if(textureUrl.startsWith("/"))
		 	    			textureUrl = textureUrl.substring(1);
		 	    		textureUrl = objFileDirFlattenedPath + textureUrl;
		 	    		textureUrl = new URI(textureUrl).normalize().toString();
		 	    		if(textureUrl.startsWith("/"))
		 	    			textureUrl = textureUrl.substring(1);
		 	    		textureUrl = textureUrl.replace("/", "__");
		 	    		textureUrl = "/api/files/" + job.getId() + "/" + textureUrl;
		 	    				
		 	    		line = line.replace(textureUrlOld, textureUrl); 	    		
		 	    	}
		 	    	fileWriter.write(line);
		 	    }
	 	    }catch(Exception e){
	 	       x3dReader.close();
	 	 	   tempFile.delete();
	 	 	   fileWriter.close();
	 	 	   outFile.delete();
	 	 	   throw new Exception("File not processed correctly. File is possibly corrupt.");
	 	    }
	 	    
	 	   x3dReader.close();
	 	   tempFile.delete();
	 	   fileWriter.close();
	 	   log.info("Flattening completed.");
	 	     	   
		   ///////////////////////
	 	   
	 	   log.info("Uploading texture files.");
	 	   callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Uploading texture files.", log);
	 	   
	 	   ArrayList<File> textureFiles = new ArrayList<File>();	    
		   searchForFilesWithExtensions(unzipDirFile, textureFiles, mtlTextureMapFormats);
	 	   //Upload service.
	 	   ExecutorService exec = Executors.newFixedThreadPool(uploadThreads); 	   
	 	   for(int i = 0; i < textureFiles.size(); i++){
	 		   // upload the file in a new thread
	 		   if (uploadThreads == 1) {
	 			   new UploadTexture(textureFiles.get(i),tempDir + unzipDirFile.getName()).run();
	 		   } else {
	 			   exec.execute(new UploadTexture(textureFiles.get(i),tempDir + unzipDirFile.getName()));
	 		   }
	 	   }       
	 	   // wait for all threads
	 	   if (uploadThreads != 1) {
	 		   exec.shutdown();
	 		   if (!exec.awaitTermination(EXEC_MAX_WAIT, TimeUnit.SECONDS)) {
	 			   int count = exec.shutdownNow().size();
	 			   log.warn(String.format("Waited more than %d seconds, killed %d jobs.", EXEC_MAX_WAIT, count));
	 		   }
	 	   }
	 	   log.info("Done uploading texture files.");
    	}catch(Exception e){
    		FileUtils.deleteDirectory(unzipDir.toFile());
    		throw e;
    	}
 	    	   
 	   FileUtils.deleteDirectory(unzipDir.toFile());

 	   return outFile;
	}
	
	public static void searchForFilesWithExtensions(File root, List<File> allowedOnly, String[] allowedExtensions)
	{
	    if(root == null || allowedOnly == null) return; //just for safety   

	    if(root.isDirectory())
	    {
	        for(File file : root.listFiles())
	        	searchForFilesWithExtensions(file, allowedOnly, allowedExtensions);
	    }
	    else if(root.isFile())
	    {
	    	for(int i = 0; i < allowedExtensions.length; i++){
	    		if(root.getName().endsWith(allowedExtensions[i]))
	    			allowedOnly.add(root);	    			    			
	    	}
	    }
	}
	
	private Float getMaxDimension(File tempFile) throws IOException {

		Float maxX = new Float(-new Float(1000000)), minX = new Float(1000000), maxY = new Float(-new Float(1000000)), minY = new Float(1000000), maxZ = new Float(-new Float(1000000)), minZ = new Float(1000000);

		BufferedReader fileReader = new BufferedReader(new FileReader(tempFile));
		String line = null;
		while ((line = fileReader.readLine()) != null){
			if(line.toLowerCase().indexOf("<coordinate") >= 0){
				line = line.replace('\"', '\'');
				String[] coords = line.substring(line.indexOf("point=")+7,  line.indexOf("\'",line.indexOf("point=")+7)).trim().split(" +");
				line = null;
				for (int i = 0; i < coords.length; i+= 3){
					Float currX = new Float(coords[i]), currY = new Float(coords[i+1]), currZ = new Float(coords[i+2]);					
					if(currX > maxX)
						maxX = currX;
					if(currX < minX)
						minX = currX;
					if(currY > maxY)
						maxY = currY;
					if(currY < minY)
						minY = currY;
					if(currZ > maxZ)
						maxZ = currZ;
					if(currZ < minZ)
						minZ = currZ;
				}
				coords = null;
			}
			line = null;
		}
		fileReader.close();

		Float maxDimension = Math.max(Math.max(maxX - minX,maxY - minY), maxZ - minZ);

		return maxDimension;
	}
	
	private File createCenteringScriptFile(File tempFile) throws IOException {
		
		File centerScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(centerScript));
		fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Transform: Move, Translate, Center\">"
						+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisX\" max=\"293.67\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisY\" max=\"293.67\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisZ\" max=\"293.67\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"centerFlag\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"Freeze\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"ToAll\"/>"
						+ "</filter></FilterScript>");
		fileWriter.close();
		
		return centerScript;
	}
	
	private File createScalingScriptFile(File tempFile) throws IOException {
		
		File scalingScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(scalingScript));
		fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Transform: Scale\">"
						+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisX\" max=\"10\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisY\" max=\"10\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisZ\" max=\"10\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"uniformFlag\"/>"
						+ "<Param enum_val0=\"origin\" enum_val1=\"barycenter\" enum_cardinality=\"3\" enum_val2=\"custom point\" type=\"RichEnum\" value=\"0\" name=\"scaleCenter\"/>"
						+ "<Param x=\"0\" y=\"0\" z=\"0\" type=\"RichPoint3f\" name=\"customCenter\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"unitFlag\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"Freeze\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"ToAll\"/>"
						+ "</filter></FilterScript>");
		fileWriter.close();
		
		return scalingScript;
	}
	
	private File createQuadricScriptFile(File tempFile, Double decimationProportion) throws IOException {
		
		File quadricScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(quadricScript));
		fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Quadric Edge Collapse Decimation\">"
						+ "<Param type=\"RichInt\" value=\"1274379378\" name=\"TargetFaceNum\"/>"
						+ "<Param type=\"RichFloat\" value=\""
						+ new DecimalFormat("#.##").format(decimationProportion)
						+ "\" name=\"TargetPerc\"/>"
						+ "<Param type=\"RichFloat\" value=\"1\" name=\"QualityThr\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveBoundary\"/>"
						+ "<Param type=\"RichFloat\" value=\"1\" name=\"BoundaryWeight\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveNormal\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"PreserveTopology\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"OptimalPlacement\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"PlanarQuadric\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"QualityWeight\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"AutoClean\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"Selected\"/>"
						+ "</filter></FilterScript>");
		fileWriter.close();
		
		return quadricScript;
	}
	
	public static BufferedImage getScaledImage(BufferedImage image, Double scalingFactor) throws IOException {

		scalingFactor = Math.sqrt(scalingFactor);
		
	    AffineTransform scaleTransform = AffineTransform.getScaleInstance(scalingFactor, scalingFactor);
	    AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);
	    
	    int imageType = image.getType();
	    if(imageType == BufferedImage.TYPE_CUSTOM)
	    	imageType = BufferedImage.TYPE_INT_ARGB;

	    return bilinearScaleOp.filter(
	        image,
	        new BufferedImage((int)(image.getWidth() * scalingFactor), (int)(image.getHeight() * scalingFactor), imageType));
	}
	
	class UploadTexture implements Runnable {

		private final File textureFile;
		private final String unzipDirPath;

		public UploadTexture(File textureFile, String unzipDirPath){
			this.textureFile = textureFile;
			this.unzipDirPath = unzipDirPath;
		}
		
		public void run() {		
			try {
				String textureId = uploadTexture();

				ObjectMapper mapper= new ObjectMapper();

				String JSONString = mapper.writeValueAsString(new FileMetadata(
						textureFile.getName(), new Long(textureFile.length()).toString()));
				
				associateTexture(textureId, JSONString);

                // delete the temp file
				textureFile.delete();
				
			 }catch (Throwable thr) {
                log.error(thr);
             }			
		}
		
		private String uploadTexture() throws ClientProtocolException, IOException {

			log.info("Uploading texture file " + textureFile.getName());
			
			String fileSeparator = System.getProperty("file.separator");
			if(fileSeparator.equals("\\"))
				fileSeparator = "\\\\";
			String textureFileFlattenedPath = textureFile.getPath().replace(unzipDirPath  + System.getProperty("file.separator") , "")
	 				   .replaceAll(fileSeparator, "__");
									
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(job.getHost() +"api/3dTextures?key="+playserverKey);
			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			entity.addPart("File", new FileBody(textureFile, textureFileFlattenedPath, "application/octet-stream", null));
            httpPost.setEntity(entity);          
			HttpResponse textureUploadResponse = httpclient.execute(httpPost);
			
			log.info(textureUploadResponse.getStatusLine());						
			HttpEntity idEntity = textureUploadResponse.getEntity();
			String json = EntityUtils.toString(idEntity);
			String textureId = json.substring(json.indexOf("\"id\":\"")+6,json.lastIndexOf("\"")-(json.indexOf("\"id\":\"")+6)+7);
			log.info("Texture file id: "+ textureId);
			
			log.info("Texture file uploaded");
						
			EntityUtils.consume(idEntity);
			EntityUtils.consume(entity);
	    	
	    	return textureId;
		}
		
		private void associateTexture(String textureId, String JSONString) throws ClientProtocolException, IOException {
			
        	log.info("Associating texture file with original file");
	    	
	    	DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(job.getHost() +"api/files/" + job.getId()
					+ "/3dTextures/" + textureId + "?key=" + playserverKey);
			httpPost.addHeader("content-type","application/json");
			StringEntity theJSON = new StringEntity(JSONString);
			
            httpPost.setEntity(theJSON);			
			HttpResponse textureUploadResponse = httpclient.execute(httpPost);
			
			log.info(textureUploadResponse.getStatusLine());
			
			EntityUtils.consume(theJSON);			
		}	
	}
	
	
	private boolean customWaitFor(Process p, int seconds){
			
			long now = System.currentTimeMillis();
			long timeoutInMillis = 1000L * seconds; 
		    long finish = now + timeoutInMillis + 30000;
		    try{
			    while ( isAlive( p ) )
			    { 
			        Thread.sleep( 10 ); 
			        if ( System.currentTimeMillis() > finish ) {
			            p.destroy();
			            return false;
			        }
			    }
			    return true;
		    }
		    catch (Exception err) {
		      err.printStackTrace();
		      return false;
		        }
		}
		public static boolean isAlive( Process p ) {  
		    try  
		    {  
		        p.exitValue();  
		        return false;  
		    } catch (IllegalThreadStateException e) {  
		        return true;  
		    } 
		}
	
}
