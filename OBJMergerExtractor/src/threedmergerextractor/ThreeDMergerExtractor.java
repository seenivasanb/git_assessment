package threedmergerextractor;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NoHttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;

/**
 * @author Constantinos Sophocleous
 */
//Used MeshLab, a tool developed with the support of the 3D-CoForm project.
public class ThreeDMergerExtractor extends Extractor {

	private static final String QUEUE_NAME = "medici_3d_obj_merger";
	private static final String CONSUMER_TAG = "medici_3d_obj_merger";
	private static final boolean DURABLE = true;
	private static final boolean EXCLUSIVE = false;
	private static final boolean AUTO_DELETE = false;

	private static String serverAddr = "";
	private static int serverPort = 5672;
	private static String messageReceived = "";

	private static Log log = LogFactory.getLog(ThreeDMergerExtractor.class);
	//Models above this size in X3D format are decimated to facilitate wwb previewing.
	//30 MB default
	private static Long decimationBound = Long.parseLong("31457280");
	
	//Models above this size in X3D format (or a generally analogous size in other formats) are decimated before processing for easier processing
	//1200 MB in X3D is default
	private static Long predecimationBound = Long.parseLong("1258291200");
	
	private String routingKey = null;
	
	public static void main(String[] args)  {

		if (args.length < 10){
			System.out.println("Input RabbitMQ server address, followed by RabbitMQ server port, RabbitMQ username, RabbitMQ password, RabbitMQ exchange name" 
								+ " ,minimum size of any model to be decimated(in megabytes), max size of model to be processed without pre-decimation (in megabytes), Medici REST API key"
								+" ,whether SSL is to be used to communicate with the RabbitMQ messaging service (true of false) and number of threads to use for extractor.");
			System.out.println("If SSL is used, optionally add the SSL trust manager certificate store path, store type and store passphrase to use certificate-based authentication.");
			return;
		}
		
		serverAddr = args[0];
		serverPort = Integer.parseInt(args[1]);
		decimationBound  = Long.parseLong(args[5]) * 1048576;
		predecimationBound = Long.parseLong(args[6]) * 1048576;
		ThreeDMergerExtractorService.setMaxSize(decimationBound);
		ThreeDMergerExtractorService.setMaxPreSize(predecimationBound);
		ThreeDMergerExtractorService.setPlayserverKey(args[7]);
		ThreeDMergerExtractorService.setUploadThreads(Integer.parseInt(args[9]));
		ThreeDMergerExtractor.setPlayserverKey(args[7]);
		ThreeDMergerExtractor.setExchangeName(args[4]);
		ThreeDMergerExtractor.setUseSsl(Boolean.parseBoolean(args[8]));
        //If SSL with certificate-based authentication is used, set the trust manager params 
        if(args.length >= 12 && ThreeDMergerExtractor.isUseSsl()){
        	ThreeDMergerExtractor.setSslTrustStorePath(args[10]);
        	ThreeDMergerExtractor.setSslTrustStoreType(args[11]);
        	ThreeDMergerExtractor.setSslTrustStorePassphrase(args[12]);
        }
		new ThreeDMergerExtractor().startExtractor(args[2], args[3]);	        
	}
	
	protected void startExtractor(String rabbitMQUsername, String rabbitMQpassword) {
		try{			 
			//Open channel and declare exchange and consumer
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(serverAddr);
			factory.setPort(serverPort);
			factory.setUsername(rabbitMQUsername);
			factory.setPassword(rabbitMQpassword);
			if(isUseSsl())
				setRabbitmqSSL(factory);
			Connection connection = factory.newConnection();

			final Channel channel = connection.createChannel();
			channel.exchangeDeclare(exchange_name, "topic", true);

			channel.queueDeclare(QUEUE_NAME,DURABLE,EXCLUSIVE,AUTO_DELETE,null);
			channel.basicQos(1);
			
			channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.obj-zipped.#");
			channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.x3d-zipped.#");

			this.channel = channel;

			// create listener
			channel.basicConsume(QUEUE_NAME, false, CONSUMER_TAG, new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					messageReceived = new String(body);
					long deliveryTag = envelope.getDeliveryTag();
					// (process the message components here ...)
					System.out.println(" {x} Received '" + messageReceived + "'");
					routingKey = envelope.getRoutingKey();
					
					replyProps = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();
					replyTo = properties.getReplyTo();
					
					if(processMessageReceived()){
			            System.out.println(" [x] Done");
		                channel.basicAck(deliveryTag, false);
	                }
		            else{
		            	System.out.println(" [x] Error processing");
		            	channel.basicNack(deliveryTag, false, true);
		            }
				}
			});

			// start listening 
			System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
			while (true) {
				Thread.sleep(1000);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}		 
	}
	
	protected boolean processMessageReceived(){
		try{
			try {
				ThreeDMergerExtractorService extrServ = new ThreeDMergerExtractorService(this);
				String modelType = routingKey.substring(routingKey.indexOf(".model.")+7, routingKey.indexOf("-zipped"));
				extrServ.setModelType(modelType);
				jobReceived = getRepresentation(messageReceived, ExtractionJob.class);
				
				if(jobReceived.isFlagSet("nopreviews")){
					log.info("Model is not to be previewed. Aborted processing of model.");
    				return true;
				}
				
				File objMergedFile = extrServ.processJob(jobReceived);
				if(objMergedFile == null){
					sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
					return true;
				}
									
				jobReceived.setFlag("3dConverted");
				jobReceived.setFlag("modelMaxDimension_" + new DecimalFormat("#.####").format(extrServ.getModelMaxDimension()).replace(".", "__"));
					
				log.info(modelType + " merging complete. Returning merged x3d file as intermediate result.");
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), modelType + " merging complete. Returning merged x3d file as intermediate result.", log);
				
				uploadIntermediate(objMergedFile, "model/x3d+xml", log);
				
				objMergedFile.delete();
				
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
				return true;

			} catch (Exception ioe) {
				log.error("Could not finish extraction job.", ioe);
				if(ioe.getMessage() != null){
					if(ioe.getMessage().contains("Server unable to process request")){						
						return false;
					}
				}
				if(ioe instanceof HttpHostConnectException || ioe instanceof SocketTimeoutException || ioe instanceof NoHttpResponseException 
					|| ioe instanceof ConnectTimeoutException || ioe instanceof UnknownHostException ){
					return false;
				}
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "Could not finish extraction job.", log);
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
				return true;
			}        
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
			return false;
		}
	}
	
}
