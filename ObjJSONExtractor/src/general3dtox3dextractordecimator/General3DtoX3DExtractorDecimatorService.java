package general3dtox3dextractordecimator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.text.DecimalFormat;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;

/**
 * @author Constantinos Sophocleous
 */
public class General3DtoX3DExtractorDecimatorService extends ExtractorService {
	
	//Maximum wait (in seconds) for meshlabserver to finish each workflow task per MB of size of 3D model file.
	private static final float CONVERSION_WAIT_PER_MB = 10.0f;
	private static final float CENTERING_WAIT_PER_MB = 10.0f;
	private static final float SCALING_WAIT_PER_MB = 10.0f;
	private static final float DECIMATION_WAIT_PER_MB = 10.0f;

	private static Log log = LogFactory.getLog(General3DtoX3DExtractorDecimatorService.class);
	
	//30 MB
	private static Long maxFileSize = Long.parseLong("31457280");
	
	//400 MB in X3D
	//Different file formats have, in general, different file sizes for same-sized actual models.
	private static Long predecimationMaxFileSizeX3D = Long.parseLong("419430400");
	private static Long predecimationMaxFileSizePLY = Long.parseLong("209715200");
	private static Long predecimationMaxFileSizeOBJ = Long.parseLong("629145600");
	private static Long predecimationMaxFileSizeOther = Long.parseLong("419430400");
	
	private String objId = null;
	
	private Float modelMaxDimension = new Float(1.0);
	
	public General3DtoX3DExtractorDecimatorService(
			General3DtoX3DExtractorDecimator general3DtoX3DExtractorDecimator) {		
		super(general3DtoX3DExtractorDecimator);		
	}

	public static void setMaxSize(Long maxSize) {
		General3DtoX3DExtractorDecimatorService.maxFileSize = maxSize;
	}
	
	public static void setMaxPreSize(Long maxPreSizeX3D) {
		General3DtoX3DExtractorDecimatorService.predecimationMaxFileSizeX3D = maxPreSizeX3D;
		General3DtoX3DExtractorDecimatorService.predecimationMaxFileSizePLY = maxPreSizeX3D / 4;
		General3DtoX3DExtractorDecimatorService.predecimationMaxFileSizeOBJ = (long) (maxPreSizeX3D * 1.5);
		General3DtoX3DExtractorDecimatorService.predecimationMaxFileSizeOther = maxPreSizeX3D;
	}
	
	public String getObjId(){
		return objId;
	}
	
	public Float getModelMaxDimension(){
		return modelMaxDimension;
	}
		
	public File processJob(ExtractionJob receivedMsg) throws Exception{
		
		log.info("Downloading 3D file with ID "+ receivedMsg.getIntermediateId() +" from " + receivedMsg.getHost());
		callingExtractor.sendStatus(receivedMsg.getId(), callingExtractor.getClass().getSimpleName(), "Downloading model file.", log);

		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getIntermediateId()+"?key="+playserverKey);		
		HttpResponse fileResponse = httpclient.execute(httpGet);
		log.info(fileResponse.getStatusLine());
		String statusString = fileResponse.getStatusLine().toString();
		if(statusString.indexOf("200") == -1){
			if(statusString.indexOf("500") >= 0 || statusString.indexOf("503") >= 0){
				throw new IOException("Server unable to process request.");
			}
			else
				throw new IOException("File not found.");
		}
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9);
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")).replace(" ", "_"), fileName.substring(fileName.lastIndexOf(".")).toLowerCase());
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();

		EntityUtils.consume(fileEntity);


		log.info("Download complete. Initiating x3d extraction");

		File x3dFile = processFile(tempFile, receivedMsg.getId());	
		return x3dFile;		
	}

	private File processFile(File tempFile, String originalFileId) throws Exception  {
			
			Runtime r = Runtime.getRuntime();
			Process p;     // Process tracks one external native process
		    String cmd;
		    File outFile;
		    StreamGobbler outputGobbler;
		    StreamGobbler errorGobbler;
		    
		    String tempDir = System.getProperty("java.io.tmpdir");
		    if (new Character(tempDir.charAt(tempDir.length()-1)).toString().equals(System.getProperty("file.separator")) == false){
		    	tempDir = tempDir + System.getProperty("file.separator");
		    }
		    
		    //Set max size to be pre-decimated to, and pre-decimate if above it.
		    Long maxFilePreSize = predecimationMaxFileSizeOther;
		    String origFileExtension = tempFile.getName().substring(tempFile.getName().lastIndexOf(".")+1);
		    if(origFileExtension.equals("x3d"))
		    	maxFilePreSize = predecimationMaxFileSizeX3D;
		    else if(origFileExtension.equals("ply"))
		    	maxFilePreSize = predecimationMaxFileSizePLY;
		    else if(origFileExtension.equals("obj"))
		    	maxFilePreSize = predecimationMaxFileSizeOBJ;		    
		    	 
		    if(!origFileExtension.equals("x3d") || tempFile.length() > maxFilePreSize){
		    	log.info("Converting model to X3D and predecimating if needed.");
		    	callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Converting model to X3D and predecimating if needed.", log);
		    	String predecimationCommand = "";
		    	File quadricScriptPre = null;
		    	if(tempFile.length() > maxFilePreSize){
		    		//Create quadric collapse decimation script.
			    	quadricScriptPre = createQuadricScriptFile(tempFile, maxFilePreSize);
			    	predecimationCommand = " -s " + tempDir + quadricScriptPre.getName();
		    	}

		    	//Pre-decimation using Quadric Edge Collapse decimation script.
		    	cmd = "meshlabserver -i " + tempDir +  tempFile.getName() + " -o " 
						+ tempDir + "x3d__" + tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")) + ".x3d"
						+ predecimationCommand
						+ " -om vc vf vq vn wt";	    
				p = r.exec(cmd);
				outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
			    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
			    outputGobbler.start();
			    errorGobbler.start();			    
			    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * CONVERSION_WAIT_PER_MB).intValue())){
			    	tempFile.delete();
			    	if(quadricScriptPre != null)
			    		quadricScriptPre.delete();
			    	throw new Exception("File not processed correctly. File is possibly corrupt.");
			    }
			    			    	
		    	File x3dFile = new File(tempDir + "x3d__" + tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")) + ".x3d");
		    	tempFile.delete();
		    	if(quadricScriptPre != null)
		    		quadricScriptPre.delete();
		    	tempFile = x3dFile;
		    	x3dFile = null;	    	
		    }
		    
		    ///////////////////////
		    
		    log.info("Centering model to center of bounding box.");
		    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Centering model to center of bounding box.", log);
	
		    //Create centering script.
		    File centerScript = createCenteringScriptFile(tempFile);
	
		    cmd = "meshlabserver -i " + tempDir +  tempFile.getName() + " -o " 
		    		+ tempDir + "center__" + tempFile.getName() + " -s " + tempDir + centerScript.getName()
		    		+ " -om vc vf vq vn wt";	    
		    p = r.exec(cmd);
		    outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
		    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
		    outputGobbler.start();
		    errorGobbler.start();
		    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * CENTERING_WAIT_PER_MB).intValue())){
		    	centerScript.delete();
		    	tempFile.delete();		    	
		    	throw new Exception("File not processed correctly. File is possibly corrupt.");
		    }
	
		    centerScript.delete();
		    File centerFile = new File(tempDir + "center__" + tempFile.getName());
		    tempFile.delete();
		    tempFile = centerFile;
		    centerFile = null;
		    
		    ///////////////////////
		    
		    log.info("Measuring model.");
		    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Measuring model.", log);
		    
		    try{
		    modelMaxDimension = getMaxDimension(tempFile);}
		    catch(Exception e){
		    	tempFile.delete();
		    	throw e;
		    }
		    
		    log.info("Max dimension of model is " +  new DecimalFormat("#.####").format(modelMaxDimension));
		    
		    ///////////////////////
		    
		    log.info("Scaling model.");
		    callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Scaling model.", log);
			
		    //Create scaling script.
		    File scalingScript = createScalingScriptFile(tempFile);
	
		    cmd = "meshlabserver -i " + tempDir +  tempFile.getName() + " -o " 
		    		+ tempDir + "scaled__" + tempFile.getName() + " -s " + tempDir + scalingScript.getName()
		    		+ " -om vc vf vq vn wt";	    
		    p = r.exec(cmd);
		    outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
		    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
		    outputGobbler.start();
		    errorGobbler.start();
		    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * SCALING_WAIT_PER_MB).intValue())){
		    	scalingScript.delete();
		    	tempFile.delete();		    	
		    	throw new Exception("File not processed correctly. File is possibly corrupt.");
		    }
	
		    scalingScript.delete();
		    File scaledFile = new File(tempDir + "scaled__" + tempFile.getName());
		    tempFile.delete();		   
		    tempFile = scaledFile;
		    scaledFile = null;
		    
		    ///////////////////////
	
		    if(tempFile.length() > maxFileSize){
		    	log.info("Decimating model.");
		    	callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Decimating model.", log);
		    	
		    	//Create quadric collapse decimation script.
		    	File quadricScript = createQuadricScriptFile(tempFile, maxFileSize);
	
		    	//Decimation using Quadric Edge Collapse decimation script.
		    	cmd = "meshlabserver -i " + tempDir +  tempFile.getName() + " -o " 
		    			+ tempDir + "dec__" + tempFile.getName() + " -s " + tempDir + quadricScript.getName()
		    			+ " -om vc vf vq vn wt";	    
		    	p = r.exec(cmd);
		    	outputGobbler = new StreamGobbler(p.getInputStream(), "INFO", log);
			    errorGobbler = new StreamGobbler(p.getErrorStream(),"ERROR", log);
			    outputGobbler.start();
			    errorGobbler.start();
			    if(!customWaitFor(p, new Double(Double.parseDouble(new Long(tempFile.length()).toString())/1024/1024 * DECIMATION_WAIT_PER_MB).intValue())){
			    	quadricScript.delete();
			    	tempFile.delete();			    	
			    	throw new Exception("File not processed correctly. File is possibly corrupt.");
			    }
	
		    	outFile = new File(tempDir + "dec__" + tempFile.getName());
		    	quadricScript.delete();
		    	tempFile.delete();		    	
		    }
		    else{
		    	outFile = tempFile;
		    }		   
	    	return outFile;
		}
	
	//Get max dimension length of model by checking the difference between min and max values of each X,Y and Z dimension.
	private Float getMaxDimension(File tempFile) throws IOException {
		
		Float maxX = new Float(-new Float(1000000)), minX = new Float(1000000), maxY = new Float(-new Float(1000000)), minY = new Float(1000000), maxZ = new Float(-new Float(1000000)), minZ = new Float(1000000);
		
		BufferedReader fileReader = new BufferedReader(new FileReader(tempFile));
		String line = null;
		while ((line = fileReader.readLine()) != null){
			if(line.toLowerCase().indexOf("<coordinate") >= 0){
				line = line.toLowerCase().replace('\"', '\'');
				
				int coordsStart = line.indexOf('\'', line.indexOf(" point"))+1;
				String coordsString = line.substring(coordsStart, line.indexOf('\'',coordsStart)).replace(",","");
				
				String[] coords = coordsString.trim().split(" +");
				line = null;
				for (int i = 0; i < coords.length; i+= 3){
					Float currX = new Float(coords[i]), currY = new Float(coords[i+1]), currZ = new Float(coords[i+2]);					
					if(currX > maxX)
						maxX = currX;
					if(currX < minX)
						minX = currX;
					if(currY > maxY)
						maxY = currY;
					if(currY < minY)
						minY = currY;
					if(currZ > maxZ)
						maxZ = currZ;
					if(currZ < minZ)
						minZ = currZ;
				}
				coords = null;
			}
			line = null;
		}
		fileReader.close();
		
		Float maxDimension = Math.max(Math.max(maxX - minX,maxY - minY), maxZ - minZ);
		
		return maxDimension;
	}

	private File createCenteringScriptFile(File tempFile) throws IOException {
		
		File centerScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(centerScript));
		fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Transform: Move, Translate, Center\">"
						+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisX\" max=\"293.67\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisY\" max=\"293.67\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"0\" min=\"-293.67\" name=\"axisZ\" max=\"293.67\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"centerFlag\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"Freeze\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"ToAll\"/>"
						+ "</filter></FilterScript>");
		fileWriter.close();
		
		return centerScript;
	}
	
	private File createScalingScriptFile(File tempFile) throws IOException {
		
		File scalingScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(scalingScript));
		fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Transform: Scale\">"
						+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisX\" max=\"10\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisY\" max=\"10\"/>"
						+ "<Param type=\"RichDynamicFloat\" value=\"1\" min=\"0.1\" name=\"axisZ\" max=\"10\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"uniformFlag\"/>"
						+ "<Param enum_val0=\"origin\" enum_val1=\"barycenter\" enum_cardinality=\"3\" enum_val2=\"custom point\" type=\"RichEnum\" value=\"0\" name=\"scaleCenter\"/>"
						+ "<Param x=\"0\" y=\"0\" z=\"0\" type=\"RichPoint3f\" name=\"customCenter\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"unitFlag\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"Freeze\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"ToAll\"/>"
						+ "</filter></FilterScript>");
		fileWriter.close();
		
		return scalingScript;
	}
	
	private File createQuadricScriptFile(File tempFile, Long fileSizeMax) throws IOException {
		
		File quadricScript = File.createTempFile(tempFile.getName().substring(0, tempFile.getName().lastIndexOf(".")), ".mlx");	
		BufferedWriter fileWriter = new BufferedWriter(new FileWriter(quadricScript));
		fileWriter.write("<!DOCTYPE FilterScript><FilterScript><filter name=\"Quadric Edge Collapse Decimation\">"
						+ "<Param type=\"RichInt\" value=\"1274379378\" name=\"TargetFaceNum\"/>"
						+ "<Param type=\"RichFloat\" value=\""
						+ new DecimalFormat("#.##").format(new Double(Double.parseDouble(fileSizeMax.toString())/ Double.parseDouble(new Long(tempFile.length()).toString()))) 
						+ "\" name=\"TargetPerc\"/>"
						+ "<Param type=\"RichFloat\" value=\"1\" name=\"QualityThr\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveBoundary\"/>"
						+ "<Param type=\"RichFloat\" value=\"1\" name=\"BoundaryWeight\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"PreserveNormal\"/>"
						+ "<Param type=\"RichBool\" value=\"true\" name=\"PreserveTopology\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"OptimalPlacement\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"PlanarQuadric\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"QualityWeight\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"AutoClean\"/>"
						+ "<Param type=\"RichBool\" value=\"false\" name=\"Selected\"/>"
						+ "</filter></FilterScript>");
		fileWriter.close();
		
		return quadricScript;
	}
	
	//Needed to create a poller to check whether external calls to MeshLab hang, as sometimes MeshLab hangs silently when called without a GUI.
	//MeshLab call is considered hung if taking time considered unreasonable in relation to 3D model size.
	private boolean customWaitFor(Process p, int seconds){
		
		long now = System.currentTimeMillis();
		long timeoutInMillis = 1000L * seconds; 
	    long finish = now + timeoutInMillis + 30000;
	    try{
		    while ( isAlive( p ) )
		    { 
		        Thread.sleep( 10 ); 
		        if ( System.currentTimeMillis() > finish ) {
		            p.destroy();
		            return false;
		        }
		    }
		    return true;
	    }
	    catch (Exception err) {
	      err.printStackTrace();
	      return false;
	        }
	}
	public static boolean isAlive( Process p ) {  
	    try  
	    {  
	        p.exitValue();  
	        return false;  
	    } catch (IllegalThreadStateException e) {  
	        return true;  
	    } 
	}
	
}



