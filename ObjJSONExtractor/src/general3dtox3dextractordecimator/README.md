# General3DtoX3DExtractorDecimator

## Overview

Generates an .x3d model for the given general 3d input model.  The generated .x3d model is centered to the center of the bounding box and scaled.  If the generated .x3d model is greater than the extractor service's maximum file size (default 30MB), the model will be decimated.
Very large models are also pre-decimated to facilitate processing.

## Input
A general 3d model to generate an .x3d model for.  The input model should have one of the following MIME types:  

* model/obj, model/ply, model/x3d+xml, model/x3d-xml, application/x-tgif, application/sla

## Output
A centered, scaled .x3d model generated from the input model.  The output .x3d model will be less than or equal to the extractor service's specified maximum file size (default 30MB).

## Dependencies
* ###Software
      
    * **MeshLab**  
   [http://meshlab.sourceforge.net/](http://meshlab.sourceforge.net/)

        $ sudo apt-get install meshlab   