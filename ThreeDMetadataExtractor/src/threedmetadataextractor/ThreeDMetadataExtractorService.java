package threedmetadataextractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;

/**
 * @author Constantinos Sophocleous
 */
public class ThreeDMetadataExtractorService extends ExtractorService {
	
	private static Log log = LogFactory.getLog(ThreeDMetadataExtractorService.class);	
	private static ObjectMapper mapper= new ObjectMapper();

	static{
		mapper.setSerializationInclusion(Include.NON_NULL);
	}
	
	public ThreeDMetadataExtractorService(ThreeDMetadataExtractor threeDMetadataExtractor) {
		super(threeDMetadataExtractor);
	}
	
	public String processJob(ExtractionJob receivedMsg) throws Exception{	
		log.info("Downloading 3D file with ID "+ receivedMsg.getIntermediateId() +" from " + receivedMsg.getHost());
		callingExtractor.sendStatus(receivedMsg.getId(), callingExtractor.getClass().getSimpleName(), "Downloading 3D file.", log);
						
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getIntermediateId()+"?key="+playserverKey);
		HttpResponse fileResponse = httpclient.execute(httpGet);
		log.info(fileResponse.getStatusLine());
		String statusString = fileResponse.getStatusLine().toString();
		if(statusString.indexOf("200") == -1){
			if(statusString.indexOf("500") >= 0 || statusString.indexOf("503") >= 0){
				throw new IOException("Server unable to process request.");
			}
			else
				throw new IOException("File not found.");
		}
		
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9);
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")), fileName.substring(fileName.lastIndexOf(".")).toLowerCase());
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();
		
		EntityUtils.consume(fileEntity);

		log.info("Download complete. Initiating metadata extraction");

		return processFile(tempFile, receivedMsg.getId());					
	}
	
	protected String processFile(File tempFile, String originalFileId) throws Exception{
		String JSONString = null;
		BufferedReader fileReader = null;
		String line = null;
		String metaArea = "";
		
		try{
			if(tempFile.getName().endsWith(".zip")){
					File actualFile = null;
					byte[] buffer = new byte[1024];
					ZipInputStream zis = null;
					FileOutputStream fos = null;
				    try {
						zis = new ZipInputStream(new FileInputStream(tempFile));
						ZipEntry ze = zis.getNextEntry();			    
						while(ze!=null){
							if(! ze.isDirectory()){			
								String fileName = ze.getName();
								if(fileName.endsWith(".x3d") || fileName.endsWith(".obj")){
									if(fileName.endsWith(".x3d"))
										actualFile = File.createTempFile("tempx3d",".x3d");
									else
										actualFile = File.createTempFile("tempobj",".obj");
									fos = new FileOutputStream(actualFile);             			    			
						    		int len;
						    		while ((len = zis.read(buffer)) > 0) {
						    			fos.write(buffer, 0, len);
						    		}			
						    		fos.close();
						    		break;
								}
							}   
							ze = zis.getNextEntry();
						}
						
						zis.closeEntry();
						zis.close();
					} catch (Exception e) {
						e.printStackTrace();						
						if(fos != null)
							fos.close();
						if(zis != null)
							zis.close();
						if(actualFile != null)
							actualFile.delete();
						throw e;
					}
			    	
			    	tempFile.delete();
			    	tempFile = actualFile;
			    	if(tempFile == null){
			    		log.error("ERROR: No 3D model file found. Aborting processing.");
			    		callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "ERROR: No 3D model file found. Aborting processing.", log);
			    	    throw new Exception("No 3D model file found.");
			    	}		    	
			}
				
			callingExtractor.sendStatus(originalFileId, callingExtractor.getClass().getSimpleName(), "Extracting metadata.", log);
			JSONObject theJSON = new JSONObject();
			if(tempFile.getName().endsWith(".x3d")){
				
				fileReader = new BufferedReader(new FileReader(tempFile));				
				//Get meta tags
				line = fileReader.readLine();
				while (line != null){
					if(line.toLowerCase().contains("<head"))
						break;
					line = fileReader.readLine();
				}
				
				if(line != null){	//<head> element exists, search for meta tags
					metaArea = metaArea.concat(line);
					if(!line.contains("</head")){
						while (!(line = fileReader.readLine().toLowerCase()).contains("</head")){	
							metaArea = metaArea.concat(line);
						}
						metaArea = metaArea.concat(line);
					}
					if(metaArea.indexOf("<meta") >= 0){
						metaArea = metaArea.substring(metaArea.indexOf("<meta"),metaArea.indexOf("</head")+1);
						String []metaTags = metaArea.split("/>");
						for(int i = 0; i < metaTags.length-1; i++){
							if(metaTags[i].contains("<meta")){
								metaTags[i] = metaTags[i].replace('\"', '\'');
								int nameStart = metaTags[i].indexOf('\'', metaTags[i].indexOf(" name"))+1;
								String metaName = metaTags[i].substring(nameStart, metaTags[i].indexOf('\'',nameStart));
								
								int contentStart = metaTags[i].indexOf('\'', metaTags[i].indexOf(" content"))+1;
								String metaContent = metaTags[i].substring(contentStart, metaTags[i].indexOf('\'',contentStart));
								
								theJSON.put(metaName, metaContent);
							}
						}
					}
				}
				else{  //<head> element does not exist, re-read file from start
					fileReader.close();
					fileReader = new BufferedReader(new FileReader(tempFile));
					line = "";
				}
				
				//Get number of vertices and number of polygons
				Integer verticesNum = 0;
				Integer polygonsNum = 0;
				if(line.indexOf("<coordinate") >= 0){
					line = line.replace('\"', '\'');				
					int coordsStart = line.indexOf('\'', line.indexOf(" point"))+1;
					String metaCoordsString = line.substring(coordsStart, line.indexOf('\'',coordsStart)).replace(",","");
					String[] coords = metaCoordsString.trim().split(" +");
					Integer coordsNum = coords.length / 3;
					verticesNum = verticesNum + coordsNum;
				}
				if(line.indexOf("<coordindex") >= 0){
					line = line.replace('\"', '\'');				
					int coordsStart = line.indexOf('\'', line.indexOf(" coordindex"))+1;
					String metaCoordsString = line.substring(coordsStart, line.indexOf('\'',coordsStart));
					String[] coords = metaCoordsString.trim().split(" +");
					Integer polygonsInShape = 0;
					for(int i = 0; i < coords.length; i++){
						if(coords[i].equals("-1")){
							polygonsInShape++;
						}
					}
					polygonsNum = polygonsNum + polygonsInShape;
				}
				while ((line = fileReader.readLine()) != null){
					if(line.toLowerCase().indexOf("<coordinate") >= 0){
						line = line.toLowerCase().replace('\"', '\'');					
						int coordsStart = line.indexOf('\'', line.indexOf(" point"))+1;
						String metaCoordsString = line.substring(coordsStart, line.indexOf('\'',coordsStart)).replace(",","");
						String[] coords = metaCoordsString.trim().split(" +");
						Integer coordsNum = coords.length / 3;
						verticesNum = verticesNum + coordsNum;
					}
					if(line.toLowerCase().indexOf(" coordindex") >= 0){
						line = line.toLowerCase().replace('\"', '\'');				
						int coordsStart = line.indexOf('\'', line.indexOf(" coordindex"))+1;
						String metaCoordsString = line.substring(coordsStart, line.indexOf('\'',coordsStart));
						String[] coords = metaCoordsString.trim().split(" +");
						Integer polygonsInShape = 0;
						for(int i = 0; i < coords.length; i++){
							if(coords[i].equals("-1")){
								polygonsInShape++;
							}
						}
						polygonsNum = polygonsNum + polygonsInShape;
					}
				}
				theJSON.put("Number of vertices", verticesNum);
				theJSON.put("Number of polygons", polygonsNum);
			}else if (tempFile.getName().endsWith(".obj")){
				fileReader = new BufferedReader(new FileReader(tempFile));
				//Get number of vertices and number of polygons
				Integer verticesNum = 0;
				Integer polygonsNum = 0;
				while ((line = fileReader.readLine()) != null){
					String theLine = line.trim().toLowerCase();
					if(theLine.startsWith("v "))
						verticesNum++;
					else if(theLine.startsWith("f ")) 
						polygonsNum++;
				}
				theJSON.put("Number of vertices", verticesNum);
				theJSON.put("Number of polygons", polygonsNum);
			}else if (tempFile.getName().endsWith(".ply")){
				fileReader = new BufferedReader(new FileReader(tempFile));
				//Get number of vertices and number of polygons
				String verticesNum = "";
				String polygonsNum = "";
				boolean foundVertices = false;
				boolean foundFaces = false;
				while ((line = fileReader.readLine()) != null){
					String theLine = line.trim().toLowerCase();
					if(theLine.startsWith("element vertex ")){
						verticesNum = theLine.substring(theLine.lastIndexOf(" ")+1);
						theJSON.put("Number of vertices", verticesNum);
						foundVertices = true;
					}
					else if(theLine.startsWith("element face ")){ 
						polygonsNum = theLine.substring(theLine.lastIndexOf(" ")+1);
						theJSON.put("Number of polygons", polygonsNum);
						foundFaces = true;
					}
					if(foundVertices && foundFaces)
						break;
				}
			}else if (tempFile.getName().endsWith(".stl")){
				fileReader = new BufferedReader(new FileReader(tempFile));
				//Get number of vertices and number of polygons
				Integer polygonsNum = 0;
				Integer verticesNum = 0;
				ArrayList<Double []> verticesCoords = new ArrayList<Double []>();
				while ((line = fileReader.readLine()) != null){
					String theLine = line.trim().toLowerCase();
					if(theLine.startsWith("facet "))
						polygonsNum++;
					else if(theLine.startsWith("vertex ")){
						String[] theCoords =  theLine.split(" +");
						verticesCoords.add(new Double[]{Double.valueOf(theCoords[1]),Double.valueOf(theCoords[2]),Double.valueOf(theCoords[3])});
					} 						
				}
				Collections.sort(verticesCoords,new Comparator<Double[]>() {
		            public int compare(Double[] coordA, Double[] coordB) {
		                if(coordA[0] < coordB[0])
		                	return -1;
		                else if(coordA[0] > coordB[0])
		                	return 1;
		                else
		                	if(coordA[1] < coordB[1])
			                	return -1;
			                else if(coordA[1] > coordB[1])
			                	return 1;
			                else
			                	if(coordA[2] < coordB[2])
				                	return -1;
			                	else if(coordA[2] > coordB[2])
				                	return 1;
			                	else
			                		return 0;
		            }
		        });
				if(verticesCoords.size() > 0){
					Double[] prev = verticesCoords.get(0);
					verticesNum = 1;
					for(int i = 1; i < verticesCoords.size(); i++){
						Double[] curr = verticesCoords.get(i);
						if(prev[0] != curr[0] || prev[1] != curr[1] || prev[2] != curr[2])
							verticesNum++;
						prev = curr;
					}					
				}
								
				theJSON.put("Number of vertices", verticesNum);
				theJSON.put("Number of polygons", polygonsNum);								
			}
			else if (tempFile.getName().endsWith(".dae")){
				fileReader = new BufferedReader(new FileReader(tempFile));
				//Get number of vertices and number of polygons
				Integer polygonsNum = 0;
				Integer verticesNum = 0;
				while ((line = fileReader.readLine()) != null){
					String theLine = line.toLowerCase().replace('\"', '\'');
					if(theLine.contains("<float_array") && theLine.contains("-positions-array")){
						int startFloatArray = theLine.indexOf("<float_array");
						int startCount = theLine.indexOf(" count",startFloatArray);
						int startNum = theLine.indexOf('\'', startCount)+1;
						int endNum = theLine.indexOf('\'', startNum+1);
						Integer verticesForShape = Integer.parseInt(theLine.substring(startNum,endNum)) / 3;
						verticesNum = verticesNum + verticesForShape;
					}
					if(theLine.contains("<triangles") || theLine.contains("<polylist") || theLine.contains("<polygons") || theLine.contains("<trifans") || theLine.contains("<tristrips")){
						int startPolygons = theLine.indexOf("<float_array");
						int startCount = theLine.indexOf(" count",startPolygons);
						int startNum = theLine.indexOf('\'', startCount)+1;
						int endNum = theLine.indexOf('\'', startNum+1);
						Integer polygonsForShape = Integer.parseInt(theLine.substring(startNum,endNum));
						polygonsNum = polygonsNum + polygonsForShape;
					}					
				}
				theJSON.put("Number of vertices", verticesNum);
				theJSON.put("Number of polygons", polygonsNum);					
			}
	
			//Get file size
			theJSON.put("File size", new DecimalFormat("#.##").format(Double.parseDouble(new Long(tempFile.length()).toString()) / 1048576) + " MB");
			
			
			JSONString = theJSON.toString();
		}catch(Exception e){
			if(fileReader != null)
				fileReader.close();
			if(tempFile != null)
				tempFile.delete();
			e.printStackTrace();
			throw new Exception("File not processed correctly. File is possibly corrupt.");
		}		
		fileReader.close();
		tempFile.delete();
		return JSONString;		
	}

}
