package threedmetadataextractor;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NoHttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;

/**
 * @author Constantinos Sophocleous
 */
public class ThreeDMetadataExtractor extends Extractor {

	private static final String QUEUE_NAME = "medici_3d_metadata";
	private static final String CONSUMER_TAG = "medici_3d_metadata";
	private static final boolean DURABLE = true;
	private static final boolean EXCLUSIVE = false;
	private static final boolean AUTO_DELETE = false;
	
	private static String serverAddr = "";
	private static int serverPort = 5672;
	private static String messageReceived = ""; 
	
	private static Log log = LogFactory.getLog(ThreeDMetadataExtractor.class);
	
	public static void main(String[] args)  {
        
        if (args.length < 7){
        	System.out.println("Input RabbitMQ server address, followed by RabbitMQ server port, RabbitMQ username, RabbitMQ password, Medici REST API key, RabbitMQ exchange name"
					+" and whether SSL is to be used to communicate with the RabbitMQ messaging service (true of false).");
			System.out.println("If SSL is used, optionally add the SSL trust manager certificate store path, store type and store passphrase to use certificate-based authentication.");
			return;
		}   
            serverAddr = args[0];
            serverPort = Integer.parseInt(args[1]);
            ThreeDMetadataExtractorService.setPlayserverKey(args[4]);
            ThreeDMetadataExtractor.setPlayserverKey(args[4]);
            ThreeDMetadataExtractor.setExchangeName(args[5]);
            ThreeDMetadataExtractor.setUseSsl(Boolean.parseBoolean(args[6]));
            //If SSL with certificate-based authentication is used, set the trust manager params 
            if(args.length >= 10 && ThreeDMetadataExtractor.isUseSsl()){
            	ThreeDMetadataExtractor.setSslTrustStorePath(args[7]);
            	ThreeDMetadataExtractor.setSslTrustStoreType(args[8]);
            	ThreeDMetadataExtractor.setSslTrustStorePassphrase(args[9]);
            }
            new ThreeDMetadataExtractor().startExtractor(args[2], args[3]);	        
    }
	
	protected void startExtractor(String rabbitMQUsername, String rabbitMQpassword) {
		 try{			 
				//Open channel and declare exchange and consumer
				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost(serverAddr);
				factory.setPort(serverPort);
				factory.setUsername(rabbitMQUsername);
				factory.setPassword(rabbitMQpassword);
				if(isUseSsl())
					setRabbitmqSSL(factory);
				Connection connection = factory.newConnection();
		    
				final Channel channel = connection.createChannel();
				channel.exchangeDeclare(exchange_name, "topic", true);
		
				channel.queueDeclare(QUEUE_NAME,DURABLE,EXCLUSIVE,AUTO_DELETE,null);
				channel.basicQos(1);
				
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.obj.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.ply.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.x3d+xml.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.x3d-xml.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.application.x-tgif.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.application.sla.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.obj-zipped.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.x3d-zipped.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.collada+xml.#");
				channel.queueBind(QUEUE_NAME, exchange_name, "*.file.model.collada-xml.#");
				
				this.channel = channel;
								
				 // create listener
		        channel.basicConsume(QUEUE_NAME, false, CONSUMER_TAG, new DefaultConsumer(channel) {
		            @Override
		            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		                messageReceived = new String(body);
		                long deliveryTag = envelope.getDeliveryTag();
		                // (process the message components here ...)
		                System.out.println(" {x} Received '" + messageReceived + "'");
		                
		                replyProps = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();
						replyTo = properties.getReplyTo();
		                
						if(processMessageReceived()){
				            System.out.println(" [x] Done");
			                channel.basicAck(deliveryTag, false);
		                }
			            else{
			            	System.out.println(" [x] Error processing");
			            	channel.basicNack(deliveryTag, false, true);
			            }
		            }
		        });
				
		        // start listening
		        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		        while (true) {
		           Thread.sleep(1000);
		        }
			}
	        catch(Exception e){
	        	e.printStackTrace();
	            System.exit(1);
	        }		 
	    }
	
	protected boolean processMessageReceived(){
    	try{
    		try {
    			ThreeDMetadataExtractorService extrServ = new ThreeDMetadataExtractorService(this);
    			jobReceived = getRepresentation(messageReceived, ExtractionJob.class);
    			if(jobReceived.isFlagSet("3dConverted")){
					log.info("Model is result of processing in Medici. Cannot extract original metadata.");
    				return true;
				}	
    			
    			String JSONString = extrServ.processJob(jobReceived);
    			
    			log.info("Metadata extraction complete. Returning metadata.");
    			sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "Metadata extraction complete. Returning metadata.", log);
    			log.info("JSON: "+ JSONString);
    			
    			associateMetadata(JSONString, log);
    			
    			sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
    			return true;
    			
			} catch (Exception ioe) {
				log.error("Could not finish extraction job.", ioe);
				if(ioe.getMessage() != null){
					if(ioe.getMessage().contains("Server unable to process request")){						
						return false;
					}
				}
				if(ioe instanceof HttpHostConnectException || ioe instanceof SocketTimeoutException || ioe instanceof NoHttpResponseException 
					|| ioe instanceof ConnectTimeoutException || ioe instanceof UnknownHostException ){
					return false;
				}
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "Could not finish extraction job.", log);
				sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
				return true;
			}        
    	}catch(Exception e){
    		e.printStackTrace();
            System.exit(1);
            return false;
    	}
    }
	
}
