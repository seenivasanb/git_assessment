# ThreeDMetadataExtractor

## Overview
Extracts metadata from an input 3D model and returns them to be associated with the model. For all model types, the file size, number of vertices and number
of polygons are calculated from the file. For X3D files, also the metadata defined explicitly in the file header as values of meta tags.

## Input

* A general 3d model of any of the types: model/obj, model/ply, model/x3d+xml, model/x3d-xml, application/x-tgif, application/sla, model/vnd.collada+xml. Alternatively
* A zip file containing a single .obj or .x3d model to be processed.  The zip can contain an arbitrary number of other files (texture files, etc).

## Output
Metadata of models in JSON format. These include at least the file size, number of vertices and number of polygons, as well as any XML meta tags on XML-based file headers.  